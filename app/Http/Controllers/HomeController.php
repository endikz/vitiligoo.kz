<?php

namespace App\Http\Controllers;


use App\Http\Requests\StoreOrderRequest;
use App\Mail\SendMailOrder;
use Illuminate\Support\Facades\Mail;

class HomeController extends Controller
{
   public function storeOrder(StoreOrderRequest $request)
   {
       Mail::to(env('MAIL_TO_ADDRESS'))->send(new SendMailOrder($request->all()));
       return redirect()->back()->with('success', 'Заявка успешно отправлена');
   }
}
