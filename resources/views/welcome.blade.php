@extends('layouts.app')

@section('content')

    <section class="works" id="works">
        <div class="container">
                <h1 class="eq-header-title">Наши работы</h1>
                <p class="eq-header-sub-text">Виды услуг и результаты процедур</p>
            <div class="gallery" id="images">
                <div class="gallery-item">
                    <img data-full="/img/works/default/06.jpg" src="/img/works/thumbs/06_thumb.jpg">
                </div>
                <div class="gallery-item">
                    <img data-full="/img/works/default/07.jpg" src="/img/works/thumbs/07_thumb.jpg">
                </div>
                <div class="gallery-item">
                    <img data-full="/img/works/default/01.jpg" src="/img/works/thumbs/01_thumb.jpg">
                </div>
                <div class="gallery-item">
                    <img data-full="/img/works/default/02.jpg" src="/img/works/thumbs/02_thumb.jpg">
                </div>
                <div class="gallery-item">
                    <img data-full="/img/works/default/03.jpg" src="/img/works/thumbs/03_thumb.jpg">
                </div>
                <div class="gallery-item">
                    <img data-full="/img/works/default/04.jpg" src="/img/works/thumbs/04_thumb.jpg">
                </div>
                <div class="gallery-item">
                    <img data-full="/img/works/default/05.jpg" src="/img/works/thumbs/05_thumb.jpg">
                </div>
                <div class="gallery-item">
                    <img data-full="/img/works/default/08.jpg" src="/img/works/thumbs/08_thumb.jpg">
                </div>
                <div class="gallery-item">
                    <img data-full="/img/works/default/09.jpg" src="/img/works/thumbs/09_thumb.jpg">
                </div>
            </div>
        </div>
    </section>
    <section class="equipments" id="equipments">
        <div class="container">
            <div class="eq-header">
                <h1 class="eq-header-title">Оборудование Kernel</h1>
                <p class="eq-header-sub-text">Общие технические характеристики</p>
                <div class="eq-params">
                    <div class="eq-param-item">
                        <img src="/img/params/skin.svg" alt="">
                        <p>Предназначен для лечения хронических кожных заболеваний в домашних условиях и в медицинских
                            центрах</p>
                    </div>
                    <div class="eq-param-item">
                        <img src="/img/params/diagonal-stretch.svg" alt="">
                        <p>Небольшой размер, удобный для транспортировки, легкость в использовании</p>
                    </div>
                    <div class="eq-param-item">
                        <img src="/img/params/protector-glasses.svg" alt="">
                        <p>Защитные очки</p>
                    </div>
                    <div class="eq-param-item">
                        <img src="/img/params/laser-cutting-machine.svg" alt="">
                        <p>Узкополосный спектр действия В лучи 311 нм</p>
                    </div>
                    <div class="eq-param-item">
                        <img src="/img/params/time.svg" alt="">
                        <p>Жидкокристаллический экран, встроенный автоматический таймер</p>
                    </div>

                    <div class="eq-param-item">
                        <img src="/img/params/clipboard.svg" alt="">
                        <p>Инструкция по применению на русском языке </p>
                    </div>
                </div>
            </div>

            <div class="products">
                <div class="product-item">
                    <div class="product-image">
                        <img src="/img/products/product4.jpg" alt="">
                    </div>

                    <div class="product-info">
                        <h5>Модель KN- 4006</h5>
                        <ul>
                            <li>Лампы Phillips- 2*9W UV-B-311 nm</li>
                            <li>Рабочая площадь: 104 см</li>
                            <li>Расстояние для лечения -3 см</li>
                            <li>Размер : 325*235*345</li>
                            <li>Мощность : 110/220 В</li>
                            <li>Гарантия 2 года</li>
                        </ul>
                        <a target="_blank" class="btn-order-blue" href="https://wa.me/77014764216">Купить</a>
                    </div>
                </div>

                <div class="product-item swap">
                    <div class="product-image">
                        <img src="/img/products/product2.png" alt="">
                    </div>

                    <div class="product-info">
                        <h5>Модель KN-4003</h5>
                        <ul>
                            <li>Лампы Phillips 1* 9W UV-B-311-nm</li>
                            <li>Рабочая площадь: 104 см</li>
                            <li>Расстояние для лечения -3 см</li>
                            <li>Размер : 325*235*345</li>
                            <li>Мощность : 110/220 В</li>
                            <li>Гарантия 2 года</li>
                        </ul>
                        <a target="_blank" class="btn-order-blue" href="https://wa.me/77014764216">Купить</a>
                    </div>
                </div>

                <div class="product-item">
                    <div class="product-image">
                        <img src="/img/products/product1.png" alt="">
                    </div>

                    <div class="product-info">
                        <h5>Кабина для ультрафиолетового облучения Kernel - KN-4001</h5>
                        <p>Предназначен для лечения распространенных форм заболеваний кожи.</p>
                        <ul>
                            <li>Лампы : Phillips -40 ламп UV-B-311 /AB/А/В</li>
                            <li>Встроенный микрокомпьютер : полный контроль дозы и времени</li>
                            <li>Инновационная система безопасности и охлаждения внутри кабины.</li>
                            <li>Мобильная стойка на колесах для свободного передвижения внутри помещения .</li>
                            <li>Размеры оборудования : 1300*1320*2200 мм</li>
                            <li>Мощность : 100 W</li>
                        </ul>
                        <p>Бесплатная установка оборудования на территории РК</p>
                        <a target="_blank" class="btn-order-blue" href="https://wa.me/77014764216">Купить</a>
                    </div>
                </div>

                <div class="product-item swap">
                    <div class="product-image">
                        <img src="/img/products/product3.png" alt="">
                    </div>

                    <div class="product-info">
                        <h5>Мобильное оборудование для ультрафиолетового облучения Kernel KN-4004</h5>
                        <p>Предназначен :</p>
                        <ul>
                            <li>для лечения детей младшего возраста</li>
                            <li>пациентам с боязнью закрытого пространства</li>
                            <li>для локальных форм кожных заболеваний</li>
                            <li>для распространенных форм кожных заболеваний</li>
                        </ul>
                        <p>Технические характеристики :</p>
                        <ul>
                            <li>Лампы – 8 UV-B-311 nm /AB/A</li>
                            <li>Компактный дизайн с двумя створками позволяет экономить место в физиокабинете</li>
                            <li>Встроенный микрокомпьютер для контроля дозы и времени лечения</li>
                            <li>Мобильная стойка на колесах для свободного передвижения внутри помещения .</li>
                            <li>Мощность : 100 W</li>
                            <li>Размер оборудования :600*140*1850 мм</li>
                        </ul>
                        <a target="_blank" class="btn-order-blue" href="https://wa.me/77014764216">Купить</a>
                    </div>
                </div>

                <div class="product-item">
                    <div class="product-image">
                        <img src="/img/products/product5.jpg" alt="">
                    </div>

                    <div class="product-info">
                        <h5>Лампа Вуда KN-9000B</h5>
                        <p>Предназначен для диагностики кожных заболеваний.</p>
                        <ul>
                            <li>Рабочее расстояние: 5 см ± 1 см</li>
                            <li>Источник света：UVA свет+белый свет</li>
                            <li>Выходная длина волны: 320нм - 400нм (Пиковая длина волны 365нм)</li>
                            <li>Срок службы лампы: ≥ 15000 часов</li>
                            <li>Время увеличения зрительных линз : 2 раза±20%</li>
                        </ul>
                        <a target="_blank" class="btn-order-blue" href="https://wa.me/77014764216">Купить</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="advantage" id="advantages">
        <div class="container">
            <h1>Почему мы?</h1>
            <div class="ad-list">
                <div class="ad-item">
                    <div class="ad-num">01</div>
                    <div class="ad-text">Официальный дистрибьютор завода Kernel в Казахстане</div>
                </div>
                <div class="ad-item">
                    <div class="ad-num">02</div>
                    <div class="ad-text">Лицензия Министерства Республики Казахстан</div>
                </div>
                <div class="ad-item">
                    <div class="ad-num">03</div>
                    <div class="ad-text">7 лет опыта лечения</div>
                </div>
                <div class="ad-item">
                    <div class="ad-num">04</div>
                    <div class="ad-text">Консультация и сопровождение дерматолога в течение всего курса лечения.
                        Индивидуальный подбор схемы лечения, учитывая возраст пациента, вид заболевания, локализацию
                        пятен, стадию и давность кожного процесса
                    </div>
                </div>
                <div class="ad-item">
                    <div class="ad-num">05</div>
                    <div class="ad-text">Гарантия оборудования 2 года</div>
                </div>
                <div class="ad-item">
                    <div class="ad-num">06</div>
                    <div class="ad-text">Бесплатное сервисное обслуживание, установка и обучение</div>
                </div>
                <div class="ad-item">
                    <div class="ad-num">07</div>
                    <div class="ad-text">Сервисный центр обслуживания в г. Нур–Султан</div>
                </div>
                <div class="ad-item">
                    <div class="ad-num">08</div>
                    <div class="ad-text">Простота, надежность и легкость в использовании</div>
                </div>
            </div>
        </div>
    </section>
    <section class="contacts" id="contacts">
        <div class="container">
            <div class="c-form-block">
                <div class="c-info">
                    <h3>Контакты</h3>
                    <div class="c-info-types">
                        <div class="c-info-type">
                            <div class="c-info-title">Телефон</div>
                            <a href="tel:+77014764216">+7 701 476 4216</a>
                        </div>
                        <div class="c-info-type">
                            <div class="c-info-title">Наши соц сети</div>
                            <div class="c-info-items">
                                <a target="_blank" href="https://www.instagram.com/dermatolog.kz">
                                    <img src="/img/instagram.svg" alt="">
                                </a>
                                <a target="_blank"  href="https://wa.me/77028513138">
                                    <img src="/img/whatsapp.svg" alt="">
                                </a>
                                <a href="mailto:info@vitiligoo.kz">
                                    <img src="/img/telegram.svg" alt="">
                                </a>
                            </div>
                        </div>
                        <div class="c-info-type">
                            <div class="c-info-title">Адрес</div>
                            <div>г. Нур-Султан, ул. Ахмета Жубанова, 24</div>
                            <div>Смирнова Ольга Михайловна</div>
                            <div>ТОО "Вита А"</div>
                        </div>
                    </div>
                </div>
                <div class="c-form">
                    <h5>Оставьте заявку прямо сейчас!</h5>
                    <p>И получите скидку 10% от стоимости товара</p>

                    <form method="post" class="form" action="{{route('order.store')}}">
                        @csrf
                        <input required name="name" type="text" class="form-input" placeholder="Имя">
                        <input required name="phone" type="text" class="form-input" placeholder="Телефон">
                        <button type="submit" class="btn-order-blue mt-3 w-30">Получить скидку</button>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('js')
    <link rel="stylesheet" href="path/to/photoswipe.css">

    <!-- Skin CSS file (styling of UI - buttons, caption, etc.)
         In the folder of skin CSS file there are also:
         - .png and .svg icons sprite,
         - preloader.gif (for browsers that do not support CSS animations) -->
    <link rel="stylesheet" href="path/to/default-skin/default-skin.css">

    <!-- Core JS file -->
    <script src="path/to/photoswipe.min.js"></script>

    <!-- UI JS file -->
    <script src="path/to/photoswipe-ui-default.min.js"></script>
@endsection
