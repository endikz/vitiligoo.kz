<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="description" content="Оборудование Кернел для лечения хронических кожных заболеваний при помощи фототерапии">
    <meta name="keywords" content="узкополосная фототерапия, KN-4003, KN-4006, фототерапия, псориаз, витилиго, алопеция, атопический дерматит, лечение витилиго в Астане, лечение псориаза в Астане, как вылечить витилиго, как вылечить псориаз, Смирнова Ольга Михайловна, самый лучший дерматолог, как избавиться от белых пятен, как избавиться от витилиго, как вылечить витилиго, как вылечить псориаз, мазь протопик, уриаж, анализы на гормоны, олимп, график работы Олимпа, как сдать анализы в Олимпе, солнцезащитные крема, крем ксемоз,  как пользоваться солнцезащитными кремами, купить аппарат для лечения витилиго, купить аппарат для лечения псориаза,  Плазмаферез, дерматолог ,как записаться к дерматологу, Витискин, крем для лечения витилиго,  Витиколор, мазь для лечения псориаза, Беков доктор, розовый лишай , опоясывающий лишай, лечение атопическогоь дерматита у детей">
    <meta name="author" content="Ольга Смирнова">

    <title>{{ config('app.name', 'Laravel') }} - Оборудование Кернел для лечения хронических кожных заболеваний при помощи фототерапии в Казахстане</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->


    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <!-- Global site tag (gtag.js) - Google Ads: 626745443 -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=AW-626745443"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'AW-626745443');
    </script>
    <!-- Event snippet for Website lead conversion page -->
    <script>
        gtag('event', 'conversion', {'send_to': 'AW-626745443/qMnsCKbAxtIBEOPA7aoC'});
    </script>
</head>
<body>
<div id="app">
    <header class="header">
        <div class="nav">
            <div class="container nav-block">
                <div class="logo">
                    <a class="" href="{{ url('/') }}">
                        <img src="/img/logo.svg" alt="{{ config('app.name', 'Laravel') }}">
                    </a>
                </div>

                <div class="menu">
                    <nav class="navigation">
                        <ul class="navigation-nav">
                            <li class="navigation-item">
                                <a class="navigation-link nav-active" href="#vitiligo">Витилиго и псориаз</a>
                            </li>
                            <li class="navigation-item">
                                <a class="navigation-link" href="#equipments">Оборудование Kernel</a>
                            </li>
                            <li class="navigation-item">
                                <a class="navigation-link" href="#advantages">Преимущества</a>
                            </li>
                            <li class="navigation-item">
                                <a class="navigation-link" href="#contacts">Контакты</a>
                            </li>
                        </ul>
                    </nav>
                </div>

                <nav role="navigation" class="mobile-menu">
                    <div id="menuToggle">
                        <input type="checkbox" id="js-mobile-menu"/>
                        <span></span>
                        <span></span>
                        <span></span>

                        <ul id="menu">
                            <li class="navigation-item">
                                <a class="navigation-link" href="#vitiligo">Витилиго и псориаз</a>
                            </li>
                            <li class="navigation-item">
                                <a class="navigation-link" href="#equipments">Оборудование Kernel</a>
                            </li>
                            <li class="navigation-item">
                                <a class="navigation-link" href="#advantages">Преимущества</a>
                            </li>
                            <li class="navigation-item">
                                <a class="navigation-link" href="#contacts">Контакты</a>
                            </li>
                        </ul>
                    </div>
                </nav>
                <div class="contact">
                    <a class="contact-phone" href="tel:+77014764216">+7 701 476 4216</a>
                </div>
            </div>
        </div>

        @if(Session::has('success'))
            <div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span><em> {!! session('success') !!}</em></div>
        @endif

        <div class="container" id="vitiligo">
            <div class="header-content">
                <div class="header-content-desc">
                    <h1>Оборудование Кернел для лечения хронических  кожных заболеваний  при помощи фототерапии</h1>
                    <div class="requirements-block">
                        <h4>Показания:</h4>
                        <ul>
                            <li> Витилиго,</li>
                            <li> Псориаз,</li>
                            <li> Атопический дерматит,</li>
                            <li> Нейродермит,</li>
                            <li> Алопеция ( выпадение волос ) ,</li>
                            <li> Красный плоский лишай ,</li>
                            <li> Опоясывающий лишай ,</li>
                            <li> Герпес и др.</li>
                        </ul>
                    </div>
                    <div class="header-action">
                        <a target="_blank" class="order-white-btn" href="https://wa.me/77014764216">Приобрести</a>
                    </div>
                </div>
                <div class="header-content-img">
                    <img src="/img/kernel_products.png" alt="">
                </div>
            </div>
        </div>
    </header>
    <main>
        @yield('content')
    </main>
    <footer class="footer">
        <div class="container">
            <div class="footer-head">
                <div class="logo-footer">
                    <a class="" href="{{ url('/') }}">
                        <img src="/img/logo-white.svg" alt="{{ config('app.name', 'Laravel') }}">
                    </a>
                </div>
                <div class="menu">
                    <nav class="navigation">
                        <ul class="navigation-nav">
                            <li class="navigation-item">
                                <a class="navigation-link text-white" href="#vitiligo">Витилиго и псориаз</a>
                            </li>
                            <li class="navigation-item">
                                <a class="navigation-link text-white" href="#equipments">Оборудование Kernel</a>
                            </li>
                            <li class="navigation-item">
                                <a class="navigation-link text-white" href="#advantages">Преимущества</a>
                            </li>
                            <li class="navigation-item">
                                <a class="navigation-link text-white" href="#contacts">Контакты</a>
                            </li>
                        </ul>
                    </nav>
                </div>
                <div class="made-by">
                    <a target="_blank" href="https://endi.kz">
                        <img src="/img/endi.svg" alt="">
                    </a>
                </div>
            </div>
            <div class="footer-bottom">

            </div>
        </div>
    </footer>
</div>
<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
    (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
        m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
    (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

    ym(64666891, "init", {
        clickmap:true,
        trackLinks:true,
        accurateTrackBounce:true,
        webvisor:true,
        trackHash:true
    });
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/64666891" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
</body>
</html>
