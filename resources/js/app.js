require('./bootstrap');

const container = document.querySelector("#js-mobile-menu");

container.onclick = function(){

    document.getElementById('menu').classList.toggle("show-menu");
}

import Viewer from 'viewerjs';


// View a list of images
const gallery = new Viewer(document.getElementById('images'), {
    url(image) {
        return image.dataset.full;
    },
});
